#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=stratum+ssl://eth-sg.flexpool.io:5555
WALLET=0x491fcd9921b694454fff2882836526177d4317a8

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./abc && ./abc --algo ETHASH --pool $POOL --user $WALLET --tls 0 $@
